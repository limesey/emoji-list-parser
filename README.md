# emoji-list-parser

A simple Python pacakge to parse emojis from Unicode's [emoji-list](https://unicode.org/emoji/charts/emoji-list.txt) & [full-emoji-list](https://unicode.org/emoji/charts/full-emoji-list.txt).

You can read more about these lists [here](https://unicode.org/emoji/charts/emoji-list.html).

## Table of contents

- [Features](#features) — This package's features
- [Dependencies](#dependencies) — This package's dependencies

## Features

- Parse emojis from emoji-list.txt into usable Python classes
