import emoji


class Parser():
    def __init__(self):
        pass

    def _parse_emoji_from_line(self, line):
        line_content = line.split("	")

        codepoints = line_content[0].split(" ")
        emoji_name = line_content[1]

        return emoji.Emoji(emoji_name, codepoints)

    def parse_from_file(self, f: str):
        """
        Parse emojis from a *emoji-list.txt

        Returns a list of <class Emoji>
        """

        emojis = []

        with open(f, "r", encoding="utf-8") as emoji_file:
            content = emoji_file.read()

            lines = content.split("\n")

            for line in lines:
                if(len(line) < 1):
                    continue

                if(line[0] == "@"):
                    continue

                emojis.append(self._parse_emoji_from_line(line))

            if not emoji_file.closed:
                emoji_file.close()

        return emojis


if __name__ == "__main__":
    emojis = Parser().parse_from_file("full-emoji-list.txt")

    the_string = ""

    for emoji in emojis:
        the_string = the_string + "," + emoji.get_unicode()

    print(the_string)
