class Emoji():
    """
    Class representing an emoji
    """

    def __init__(self, name, codepoints: list):
        self.name = name
        self._unicode = ""
        self._codepoints = codepoints

    def _to_unicode(self):
        emoji_unicode = ""

        for codepoint in self._codepoints:
            emoji_unicode = emoji_unicode + \
                self._codepoint_to_unicode(codepoint)

        self.unicode = emoji_unicode

        return emoji_unicode

    def _codepoint_to_unicode(self, codepoint: str):
        """
        Util function used to convert a codepoint into a unicode character
        """
        if codepoint == " ":
            return ""

        return chr(int(codepoint, 16))

    def get_unicode(self):
        """
        Returns the emoji's unicode

        Use get_unicode() to ensure that you get a unicode character instead of an empty string
        """

        if(self._unicode == ""):
            return self._to_unicode()

        return self.unicode
